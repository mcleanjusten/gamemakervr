﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VREditor_HandleController : MonoBehaviour {
	
	public Transform selectedObject;
	public VREditor_Tools tools;
	public ObjectMenuController objectController;
	public float rotationSensitivity = 45;
	public float scaleSensitivity = 2;
	
	private Vector3 workerVec;
	private Vector3 lastPos;
	private Vector3 firstPos;
	public Vector3 startScale;
	
	public void UpdateControllerPosition (Vector3 position, int axis) {
		switch (tools.CurrentTool()) {
			case 1: // Move
				switch (axis) {
					case 0: // X
						if (objectController.useLocalSpace) {
							//selectedObject.transform.localPosition += new Vector3(transform.InverseTransformPoint(position - lastPos).x, 0, 0);
							selectedObject.transform.position += selectedObject.forward*(transform.InverseTransformPoint(position).x - transform.InverseTransformPoint(lastPos).x);
						} else {
							selectedObject.transform.position += new Vector3((position - lastPos).x, 0, 0);
						}
						break;
					case 1: // Y
						if (objectController.useLocalSpace) {
							//selectedObject.localPosition += new Vector3(0, transform.InverseTransformPoint((position - lastPos)).y, 0);
							selectedObject.position += selectedObject.forward*(transform.InverseTransformPoint(position).y - transform.InverseTransformPoint(lastPos).y);
						} else {
							selectedObject.position += new Vector3(0, (position - lastPos).y, 0);
						}
						break;
					case 2: // Z
						if (objectController.useLocalSpace) {
							//selectedObject.localPosition += new Vector3(0, 0, transform.InverseTransformPoint((position - lastPos)).z);
							selectedObject.position += selectedObject.forward*(transform.InverseTransformPoint(position).z - transform.InverseTransformPoint(lastPos).z);
						} else {
							selectedObject.position += new Vector3(0, 0, (position - lastPos).z);
						}
						break;
					case 3: // FreeForm
						selectedObject.position += (position - lastPos);
						break;
				}
				break;
			case 2: // Rotate
				workerVec = selectedObject.eulerAngles;
				switch (axis) {
					case 0: // X
						if (objectController.useLocalSpace) {
							selectedObject.localEulerAngles += new Vector3(rotationSensitivity*(transform.InverseTransformPoint(position).z - transform.InverseTransformPoint(lastPos).z), 0, 0);
						} else {
							selectedObject.Rotate(new Vector3(rotationSensitivity*(position - lastPos).z, 0, 0), Space.World);
						}
						break;
					case 1: // Y
						if (objectController.useLocalSpace) {
							selectedObject.localEulerAngles += new Vector3(0, rotationSensitivity*(transform.InverseTransformPoint(position).x - transform.InverseTransformPoint(lastPos).x), 0);
						} else {
							selectedObject.Rotate(new Vector3(0, rotationSensitivity*(position - lastPos).x, 0), Space.World);
						}
						break;
					case 2: // Z
						if (objectController.useLocalSpace) {
							selectedObject.localEulerAngles += new Vector3(0, 0, rotationSensitivity*(transform.InverseTransformPoint(position).x - transform.InverseTransformPoint(lastPos).x));
						} else {
							selectedObject.Rotate(new Vector3(0, 0, rotationSensitivity*(position - lastPos).x), Space.World);
						}
						break;
				}
				break;
			case 3: // Scale
				print ("Delta Pos " + transform.InverseTransformPoint(position - firstPos));
				switch (axis) {
					case 0: // X
						selectedObject.localScale = startScale + new Vector3(scaleSensitivity*(transform.InverseTransformPoint(position).x - transform.InverseTransformPoint(firstPos).x), 0, 0);
						break;
					case 1: // Y
						selectedObject.localScale = startScale + new Vector3(0, scaleSensitivity*(transform.InverseTransformPoint(position).y - transform.InverseTransformPoint(firstPos).y), 0);
						break;
					case 2: // Z
						selectedObject.localScale = startScale + new Vector3(0, 0, scaleSensitivity*(transform.InverseTransformPoint(position).z - transform.InverseTransformPoint(firstPos).z));
						break;
				}
				break;
		}
		lastPos = position;
	}
	
	public void SetLastPos (Vector3 pos) {
		firstPos = pos;
		startScale = selectedObject.localScale;
		lastPos = pos;
	}
}
