﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class VREditor_Tools : MonoBehaviour {
	
	/* Tool Indicies
	0 - Grab (Move AND Rotate freely, no handles)
	1 - Move
	2 - Rotate
	3 - Scale
	*/
	
	public GameObject lineHandles;
	public GameObject positionHandles;
	public GameObject scaleHandles;
	public GameObject rotationHandles;
	private int toolIndex;
	
	public void ChangeTool (int index) {
		toolIndex = index;
		switch(toolIndex) {
			case 0: // Free Form
				lineHandles.SetActive(false);
				positionHandles.SetActive(false);
				scaleHandles.SetActive(false);
				rotationHandles.SetActive(false);
				break;
			case 1: // Move
				lineHandles.SetActive(true);
				positionHandles.SetActive(true);
				scaleHandles.SetActive(false);
				rotationHandles.SetActive(false);
				break;
			case 2: // Rotate
				lineHandles.SetActive(false);
				positionHandles.SetActive(false);
				scaleHandles.SetActive(false);
				rotationHandles.SetActive(true);
				break;
			case 3: // Scale
				lineHandles.SetActive(true);
				positionHandles.SetActive(false);
				scaleHandles.SetActive(true);
				rotationHandles.SetActive(false);
				break;
		}
	}
	
	public int CurrentTool () {
		return toolIndex;
	}
	
	public void PerformUndo () {
		if (Undo.GetCurrentGroup() < 0)
			return;
		Undo.PerformUndo();
	}
	
	public void PerformRedo () {
		Undo.PerformRedo();
	}
}
