﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class ControllerInteraction : MonoBehaviour
{

    public VREditorController mainController;
    public LineRenderer line;
	public VREditor_Tools tools;
	public ObjectMenuController objectController;
	public Transform mainMenu;
	public VREditor_Controller_UI controllerUI;

    private RaycastHit hit;

    private SteamVR_TrackedController controller;

    private bool grabbing;
    private Transform grabbedTransform;
    private Transform parentOfGrabbed;

    private bool saving;
    private bool padPressed;
	private Vector3 originalPosition;
	private Quaternion originalRotation;
	private Vector3 originalScale;
	private Vector3 curPos;
	private Quaternion curRot;
	private Vector3 curScale;
	
	private bool grabbingHandles;
	private GameObject grabbedHandle;

    // Generic Initialization
    void Start()
    {
        controller = GetComponent<SteamVR_TrackedController>();
        line.enabled = false;
    }

    void Update()
    {
        // Recenter Menu
        if (controller.menuPressed) {
            mainMenu.position = transform.position + transform.forward*5;
        }
		
		// If grabbing handles, do the stuff here
		if (grabbingHandles && controller.triggerPressed) {
			grabbedHandle.SendMessage("UpdateControllerPosition", transform.position);
			return;
		} else if (grabbingHandles) {
			grabbingHandles = false;
			grabbedHandle = null;
		}
		
        // Do UI interaction
        line.SetPosition(0, transform.position);
        // Check if we're pointing at a GameMakerVR_UI element
        if (Physics.Raycast(transform.position, transform.forward, out hit) && LayerMask.LayerToName(hit.transform.gameObject.layer) == "GameMakerVR_UI")
        {
            // Make sure the controller is not pointing to the menu attatched to itself
            if(! ObjectMenuController.objectMenuController.isEnabled || ObjectMenuController.objectMenuController.current_controller != controller)
            {
                // If so, draw a line from the controller to the spot on the UI we're aiming at
                line.enabled = true;
                line.SetPosition(1, hit.point);
                if (controller.triggerPressed && hit.transform.GetComponent<Button>())
                { // If we click the thumbpad and are aimed at a button, Invoke that button's onClick functions
                    if (!padPressed)
                    {
                        padPressed = true;
                        hit.transform.GetComponent<Button>().onClick.Invoke();
                    }
                } else if (controller.triggerPressed) {
					if (!padPressed)
                    {
                        padPressed = true;
                        hit.transform.gameObject.SendMessage("VREditorClicked", hit.point, SendMessageOptions.DontRequireReceiver);
                    }
					hit.transform.gameObject.SendMessage("VREditorConstantClicked", hit.point, SendMessageOptions.DontRequireReceiver);
                } else if (padPressed)
                    padPressed = false;
            }
            else
            {
                line.enabled = false;
            }

            
        }
        // Check if we're pointing at a Game Object
        else if (Physics.Raycast(transform.position, transform.forward, out hit) && LayerMask.LayerToName(hit.transform.gameObject.layer) == "GameMakerVR_Interactable")
        {
            // If so, draw a line from the controller to the spot on the UI we're aiming at
            line.enabled = true;
            line.SetPosition(1, hit.point);
            // If the trigger is clicked and the controller is aimed at an interactable object and select is the current tool enable the object menu
            if (controller.triggerPressed && !controllerUI.GrabMode()) // Check for objectController's current tool needs to be added here
            { // If we click the thumbpad and are aimed at a button, Invoke that button's onClick functions
                if (!padPressed)
                {
                    padPressed = true;
                    Debug.Log("Enabling the object menu");
                    ObjectMenuController.objectMenuController.InvokeObjectMenu(hit.transform.gameObject);                   // Enable the menu for the selected object
                }
            }
            else if (padPressed)
                padPressed = false;
        }
        else { // If we're not aiming at a GameMakerVR_UI or GameMakerVR_Interactable, turn the line renderer off
            line.enabled = false;
        }
    }

    // Grab a thing
    void OnTriggerStay(Collider col)
    {
		if (!controllerUI.GrabMode() || grabbingHandles)
			return;
		if (tools.CurrentTool() == 0 && LayerMask.LayerToName(col.gameObject.layer) == "GameMakerVR_Interactable") {
			if (controller.triggerPressed)
			{ // If we're over an object and the trigger is held
				if (grabbing) // If we're already grabbing something, ignore everything
					return;
				grabbedTransform = col.transform.parent; // Store the grabbed object in a variable
				originalPosition = grabbedTransform.position;
				originalRotation = grabbedTransform.rotation;
				originalScale = grabbedTransform.localScale;
				if (grabbedTransform.gameObject.GetComponent<Rigidbody>())
				{ // If the object has a rigidbody, set it to kinematic so it doesn't physicate everywhere
					grabbedTransform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
				}
				if (grabbedTransform.parent) // If the object already has a parent, store it for safe keeping when we release this object back to its family
					parentOfGrabbed = grabbedTransform.parent;
				grabbedTransform.parent = transform; // Set the object's new parent to this controller
				grabbing = true;
			}
			else { // If we're no longer holding the trigger button, let go.
				if (grabbing)
				{ // But only if we were grabbing something to begin with.
					grabbing = false;
					
					if (grabbedTransform.parent) // Set its parent to the stored parent
						grabbedTransform.parent = parentOfGrabbed;
					else
						grabbedTransform.parent = null; // Or set it to null if there was no parent to begin with.
					if (grabbedTransform.gameObject.GetComponent<Rigidbody>())
					{ // If it has a rigidbody, turn it back to non-kinematic.
						grabbedTransform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
					}
					
					// Write an entry to the Undo stack
					curPos = grabbedTransform.position;
					curRot = grabbedTransform.rotation;
					curScale = grabbedTransform.localScale;
					grabbedTransform.position = originalPosition;
					grabbedTransform.rotation = originalRotation;
					grabbedTransform.localScale = originalScale;
					Undo.IncrementCurrentGroup();
					Undo.RecordObject(grabbedTransform, "Updated Transform");
					//Undo.IncrementCurrentGroup();
					grabbedTransform.position = curPos;
					grabbedTransform.rotation = curRot;
					grabbedTransform.localScale = curScale;
					
					// Nullify the variables so it's all good to grab again.
					grabbedTransform = null;
					parentOfGrabbed = null;
				}
			}
		} else if (controller.triggerPressed && !grabbingHandles && LayerMask.LayerToName(col.gameObject.layer) != "GameMakerVR_Interactable") {
			grabbingHandles = true;
			grabbedHandle = col.transform.gameObject;
			grabbedHandle.SendMessage("SetLastPos", transform.position);
		}
    }
}