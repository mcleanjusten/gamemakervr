﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirMove : MonoBehaviour {
    public SteamVR_TrackedController _leftController;
    public SteamVR_TrackedController _rightController;
    public Transform _player;
    public bool Rgripped = false;
    public bool Lgripped = false;
    public Vector3 RpreviousPos;
    public Vector3 LpreviousPos;
    public Vector3 RinitialPos;
    public Vector3 LinitialPos;
    float currentDistance;
    float initialDistance;
    float ratio = 1;
    int scaleStart = 0;
    bool jMoveStart = false;
    float scaleInitial;
    Vector3 newPosition;
    // Update is called once per frame
    void Update () {
		if (!Rgripped && _rightController.gripped) {
			RpreviousPos = _rightController.transform.localPosition;
		}
        if (_rightController.gripped)
        {
            Rgripped = true;
            if (_leftController.gripped) {
                Lgripped = false;

                if (scaleStart == 0){
                    RinitialPos = _rightController.transform.localPosition;
                    LinitialPos = _leftController.transform.localPosition;
                    scaleInitial = _player.localScale.x;
                    
                }
                scaleStart++;
 

                initialDistance = Vector3.Distance(RinitialPos, LinitialPos);
                currentDistance = Vector3.Distance(_rightController.transform.localPosition, _leftController.transform.localPosition);

                if(initialDistance == currentDistance)
                {
                    ratio = 1;
                }
                else
                {
                    ratio = scaleInitial * (initialDistance / currentDistance);

                }




                _player.localScale = new Vector3(ratio, ratio, ratio);
                


            } else { //One hand Airgrip move

                /*

                 newPosition = ratio*(_rightController.transform.localPosition - RpreviousPos);
                 newPosition = new Vector3(newPosition.x, newPosition.y* (-1), newPosition.z );
                 _player.position += newPosition;
                scaleStart = 0;

                */

                //One hand joystickMove
                if (jMoveStart == false)
                {
                    RpreviousPos = _rightController.transform.localPosition;
                    jMoveStart = true;
                }

                newPosition = (ratio/25) * (RpreviousPos - _rightController.transform.localPosition);
                newPosition = new Vector3(newPosition.x, newPosition.y * (-1), newPosition.z);
                _player.position += newPosition;




            }

            //RpreviousPos = _rightController.transform.localPosition;
            return;
        } else if (Rgripped) {
			Rgripped = false;
            jMoveStart = false;
            scaleStart = 0;

        }
		
		if (!Lgripped && _leftController.gripped) {
			LpreviousPos = _leftController.transform.localPosition;
		}
		if (_leftController.gripped) {
			Lgripped = true;

            /*

            newPosition = ratio*(_leftController.transform.localPosition - LpreviousPos);
            newPosition = new Vector3(newPosition.x, newPosition.y * (-1), newPosition.z );
        _player.position += newPosition;
        LpreviousPos = _leftController.transform.localPosition;

        */
            if (jMoveStart == false)
            {
                LpreviousPos = _leftController.transform.localPosition;
                jMoveStart = true;
            }

            newPosition = (ratio/25) * (LpreviousPos - _leftController.transform.localPosition);
            newPosition = new Vector3(newPosition.x, newPosition.y * (-1), newPosition.z);
            _player.position += newPosition;




        }
        else if (Lgripped) {
			Lgripped = false;
            jMoveStart = false;

        }
    }
}
