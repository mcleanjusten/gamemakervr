﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VREditor_ScrollBar : MonoBehaviour {
	
	public Scrollbar scrollbar;
	public ScrollRect scrollRect;
	public bool vertical;
	public float range;
	
	void VREditorClicked (Vector3 point) {
		if (vertical) {
			scrollbar.value = 1-Mathf.Clamp((transform.position.y - point.y), 0, range)/range;
			scrollRect.verticalNormalizedPosition = scrollbar.value;
		} else {
			scrollbar.value = 1-Mathf.Clamp((transform.position.x - point.x), 0, range)/range;
			scrollRect.horizontalNormalizedPosition = scrollbar.value;
		}
	}
	
	void VREditorConstantClicked (Vector3 point) {
		if (vertical) {
			scrollbar.value = 1-Mathf.Clamp((transform.position.y - point.y), 0, range)/range;
			scrollRect.verticalNormalizedPosition = scrollbar.value;
		} else {
			scrollbar.value = 1-Mathf.Clamp((transform.position.x - point.x), 0, range)/range;
			scrollRect.horizontalNormalizedPosition = scrollbar.value;
		}
	}
}