﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VREditor_Handles : MonoBehaviour {
	
	public VREditor_HandleController handleController;
	
	public enum HandleType { Movement, Rotation, Scale, All };
	public HandleType handleType;
	
	public enum Axis { X, Y, Z, All };
	public Axis axis;
	
	void UpdateControllerPosition (Vector3 position) {
		handleController.UpdateControllerPosition(position, (int)axis);
	}
	
	void SetLastPos (Vector3 pos) {
		handleController.SetLastPos(pos);
	}
}
