﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VREditorInteractable : MonoBehaviour {
	void Update () {
		if (Application.isEditor) { // Make sure this doesn't make it to build
            if (!Application.isPlaying) { // If we are in EDIT Mode, seppuku
				DestroyImmediate(gameObject);
			}
		} else { // If this script somehow made it to build, seppuku
			Destroy(gameObject);
		}
	}
}
