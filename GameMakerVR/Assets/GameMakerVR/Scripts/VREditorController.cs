﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;
using System;
using System.IO;

[ExecuteInEditMode]
public class VREditorController : MonoBehaviour
{

    // Custom holder for the Folder Button and the Holder for all buttons pertaining to that folder
    public struct FolderStuff
    {
        public GameObject folderBit;
        public GameObject linkedToys;
    }

    // Custom holder for each button and the gameObject it links to.
    // Will have another custom struct for materials/textures/audio/etc.
    // OR, might make this one generic for simplicity.
    // Only issue with making this one Generic is would then need a Switch or similar code to determine what to do for each.
    public struct GameObjectHolder
    {
        public GameObject button;
        public GameObject gameObject;
    }

    // Custom holder for each button and the Material it links to.
    public struct MaterialHolder
    {
        public GameObject button;
        public Material material;
    }

    [Header("SetUp")]
    public GameObject saveHelper;
    public GameObject saveHelperPrefab;
    public GameObject grabberHelper;
    public Transform playerView;
    public Transform mainMenu;
	public ObjectMenuController objectController;

    [Header("Omissions")]
    public string[] omittedObjectList;
    public string[] omittedFolderList;

    [Header("UI")]
    public Transform directoryList;
    public GameObject templateDirectoryButton;
    public float directorySpacing;
    public Transform toyList;
    public Transform storedToys;
    public GameObject templateToyButton;
    public float toySpacing;
	public Text globalLocalText;

    private GameObject[] world;
    private List<string> folders;
    private bool initialized;
    private GameObject workerGO;
    private Transform workerTransform;
    private FolderStuff[] folderStuffs;
    private GameObjectHolder workerGOHolder;
    private List<GameObjectHolder> goHolder;
    private MaterialHolder workerMatHolder;
    private List<MaterialHolder> matHolder;
	private bool layersInitialized;
	private int maxLayers = 31;
	private List<int> workerIntList;

    void Update()
    {
        if (Application.isEditor)
        { // In case this script makes it into the final game, DON'T RUN IN BUILD!
            if (!Application.isPlaying)
            { // If we are in EDIT Mode, check if we need to finalize a save
                if (saveHelperPrefab.transform.childCount != 0)
                {
                    ApplySave(); // Do that then.
                }
				
				if (!layersInitialized) {
					// Initialize all required layers for GameMakerVR here
					layersInitialized = true;
					
					workerIntList = new List<int>();
					
					for(int i = 0; i < maxLayers; i++) {
						if(LayerMask.LayerToName(i).Length>0) // If layer i has a name, add it to the list.
							workerIntList.Add(i);
					}

					AddLayer("GameMakerVR_Interactable", workerIntList.ToArray(), true);
					AddLayer("GameMakerVR_Controller", workerIntList.ToArray(), true);
					AddLayer("GameMakerVR_UI");
					
					workerIntList.Clear();
					for(int i = 0; i < maxLayers; i++) {
						if(LayerMask.LayerToName(i).Length>0 && LayerMask.LayerToName(i) != "GameMakerVR_Controller") // All layers except the controllers
							workerIntList.Add(i);
					}
					AddLayer("GameMakerVR_Handle", workerIntList.ToArray(), true);
				}
            }
            else { // Otherwise, meaning we are in PLAY Mode
                if (!initialized) { // If we need to initialize the UI, do that then, set up the buttons and such.
					InitializeInteractions();
                    InitializeUI();
                    initialized = true;
                }
				mainMenu.LookAt(playerView);
            }
        }
    }
	
	void InitializeInteractions () {
		world = SceneManager.GetActiveScene().GetRootGameObjects(); // Make sure to ONLY grab the ROOT objects.
		
        // Set up everything that isn't the GameMakerVR object
        foreach (GameObject g in world)
        {
            if (g.tag == "GameMakerVR")
                continue;
            SpawnHelper(g);
        }
	}

    void InitializeUI()
    {
        // Get a list of all Root Folders in the project
        folders = new List<string>();
        string[] rootFolders = Directory.GetDirectories(Application.dataPath);
        foreach (string s in rootFolders)
        { // Process each Root
            if (Array.IndexOf(omittedFolderList, s.Replace(Application.dataPath, "")) < 0) // Check if the folder in question is in the omitted list
                ProcessDirectory(s); // Do process
        }
        folderStuffs = new FolderStuff[folders.Count];
        goHolder = new List<GameObjectHolder>();
        matHolder = new List<MaterialHolder>();

        // Set up all the buttons for each folder and all objects inside each folder
        for (int i = 0; i < folders.Count; i++)
        {
            // Folder Button
            int folderIndex = i; // This strange line MUST be done this way for the onClick.AddListener function to work.  MUST be a variable defined locally (Inside the loop)
            workerGO = (GameObject)Instantiate(templateDirectoryButton, Vector3.zero, Quaternion.identity); // Make the Folder Button
            workerGO.name = folders[i].Replace(Application.dataPath, ""); // Name the Folder Button as the name of the folder path INSIDE of the Assets folder
            workerGO.SetActive(true);
            workerGO.transform.parent = directoryList;
            workerGO.transform.localScale = new Vector3(1, 1, 1);
            workerGO.transform.localPosition = new Vector3(90, -23 - directorySpacing * i, 0); // Set position and spacing
            workerGO.transform.GetChild(0).GetComponent<Text>().text = folders[i].Replace(Application.dataPath, ""); // Set the text to the same as the name
            workerGO.GetComponent<Button>().onClick.AddListener(() => OpenFolder(folderIndex)); // Add the Button onClick Listener
            folderStuffs[i].folderBit = workerGO; // Link it to the list of folders

            // Set up the Holder for the Object Buttons of what's inside this folder
            workerTransform = new GameObject().transform;
            workerTransform.parent = toyList;
            workerTransform.localScale = new Vector3(-1, 1, 1);
            workerTransform.localPosition = Vector3.zero;
            workerTransform.gameObject.name = folders[i].Replace(Application.dataPath, ""); // Name it the same as the folder name.
            folderStuffs[i].linkedToys = workerTransform.gameObject; // Link it to the same item in the list of folders

            GameObject[] gameObjects = GetAtPath<GameObject>(folders[i]); // Get all objects of type GameObject inside this specific folder
            for (int j = 0; j < gameObjects.Length; j++)
            {
                if (Array.IndexOf(omittedObjectList, gameObjects[j].name) >= 0) // If this specific GameObject is on the list of omitted objects, continue to the next object.
                    continue;
                // Create the GameObject button
                GameObject tempGO = (GameObject)Instantiate(templateToyButton, Vector3.zero, Quaternion.identity);
                tempGO.name = gameObjects[j].name;
                tempGO.SetActive(true);
                tempGO.transform.parent = workerTransform;
                tempGO.transform.localScale = new Vector3(1, 1, 1);
                tempGO.transform.localPosition = new Vector3(60 + toySpacing * j, -60, 0); // Set position and spacing
				
				// Since Previews are loaded asyncronously, we must run a coroutine to load them in as they are... well... loaded in.
				LoadPreview(gameObjects[j], tempGO.GetComponent<RawImage>().texture);

                tempGO.transform.GetChild(0).GetComponent<Text>().text = gameObjects[j].name;
                tempGO.GetComponent<Button>().onClick.AddListener(() => SpawnObject(tempGO)); // Set the listener of the button

                // Set up the holder for the stuff pertaining to this button.  Aka, this button and the GameObject it points to.
                workerGOHolder = new GameObjectHolder();
                workerGOHolder.button = tempGO;
                workerGOHolder.gameObject = gameObjects[j];
                goHolder.Add(workerGOHolder);
            }
			
			Material[] materials = GetAtPath<Material>(folders[i]); // Get all objects of type Material inside this specific folder
            for (int j = 0; j < materials.Length; j++)
            {
                // Create the Material button
                GameObject tempGO = (GameObject)Instantiate(templateToyButton, Vector3.zero, Quaternion.identity);
                tempGO.name = materials[j].name;
                tempGO.SetActive(true);
                tempGO.transform.parent = workerTransform;
                tempGO.transform.localScale = new Vector3(1, 1, 1);
                tempGO.transform.localPosition = new Vector3(60 + toySpacing * j, -60, 0); // Set position and spacing
				
				// Since Previews are loaded asyncronously, we must run a coroutine to load them in as they are... well... loaded in.
				LoadPreview(materials[j], tempGO.GetComponent<RawImage>().texture);
				
                tempGO.transform.GetChild(0).GetComponent<Text>().text = materials[j].name;
                tempGO.GetComponent<Button>().onClick.AddListener(() => SpawnObject(tempGO)); // Set the listener of the button
				
                // Set up the holder for the stuff pertaining to this button.  Aka, this button and the Material it points to.
                workerMatHolder = new MaterialHolder();
                workerMatHolder.button = tempGO;
                workerMatHolder.material = materials[j];
                matHolder.Add(workerMatHolder);
            }

            // Finalize the buttons for this folder then deactivate them.
            workerTransform.parent = storedToys;
            workerTransform.gameObject.SetActive(false);
        }
    }
	
	IEnumerator LoadPreview (UnityEngine.Object assetToLoad, Texture textureToApply) {
		while (AssetPreview.IsLoadingAssetPreview(assetToLoad.GetInstanceID())) {
			yield return null;
		}
		textureToApply = (Texture)AssetPreview.GetAssetPreview(assetToLoad);
	}

    // Get all sub-folders of the root folder.
    void ProcessDirectory(string folder)
    {
        folders.Add(folder);
        string[] subFolders = Directory.GetDirectories(folder);
        foreach (string s in subFolders)
        {
            if (Array.IndexOf(omittedFolderList, s.Replace(Application.dataPath, "")) < 0) // So long as the folder isn't on the omitted list.
                ProcessDirectory(s);
        }
    }
	
	void SpawnHelper (GameObject attachedGameObject) {
		// Spawn the Grab Helper so the object can be manipulated
		workerTransform = ((GameObject)Instantiate(grabberHelper, Vector3.zero, Quaternion.identity)).transform;
		workerTransform.parent = attachedGameObject.transform;
		workerTransform.localPosition = Vector3.zero;
		workerTransform.localRotation = Quaternion.identity;
		if (attachedGameObject.GetComponent<MeshFilter>())
		{
			// If the gameObject has a mesh attached, set the Grab Box to the size of the mesh
			workerTransform.gameObject.GetComponent<BoxCollider>().center = attachedGameObject.GetComponent<MeshFilter>().sharedMesh.bounds.center;
			workerTransform.gameObject.GetComponent<BoxCollider>().size = attachedGameObject.GetComponent<MeshFilter>().sharedMesh.bounds.size;
		}
	}

    public void SpawnObject(GameObject button)
    {
        // Find the goHolder of the button.
        foreach (GameObjectHolder g in goHolder)
        {
            if (g.button == button)
            {
                // Spawn the object pertaining to that button
                workerGO = (GameObject)Instantiate(g.gameObject, playerView.position + playerView.forward * 1f, Quaternion.identity);
				
				// Spawn the helper object to make it interactable
                SpawnHelper(workerGO);
                break;
            }
        }
    }
	
    // Called when a folder button is clicked, displays all objects inside the clicked folder.
    public void OpenFolder(int index)
    {
        // Disable all folder button displays
        foreach (FolderStuff f in folderStuffs)
        {
            f.linkedToys.SetActive(false);
            f.linkedToys.transform.parent = storedToys;
        }

        // Enable just the relevant one
        folderStuffs[index].linkedToys.SetActive(true);
        folderStuffs[index].linkedToys.transform.parent = toyList;
        folderStuffs[index].linkedToys.transform.localPosition = Vector3.zero;
    }
	
	public void ToggleGlobalLocal () {
		objectController.useLocalSpace = !objectController.useLocalSpace;
		globalLocalText.text = (objectController.useLocalSpace ? "Local" : "Global");
	}

    // Saves all changes to the SaveHelper prefab to be finalized once PLAY mode has ended
    public void Save()
    {
        saveHelper.SetActive(true);
        world = SceneManager.GetActiveScene().GetRootGameObjects(); // Make sure to ONLY grab the ROOT objects.  We don't need to move or touch childed objects.

        // Clear out the Save Buffer (SaveHelper Prefab)
        for (int i = saveHelper.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(saveHelper.transform.GetChild(i).gameObject);
        }

        // Put everything in there that isn't the GameMakerVR prefab
        foreach (GameObject g in world)
        {
            if (g.tag == "GameMakerVR")
                continue;
            g.transform.parent = saveHelper.transform;
        }
        PrefabUtility.ReplacePrefab(saveHelper, saveHelperPrefab, ReplacePrefabOptions.ConnectToPrefab | ReplacePrefabOptions.ReplaceNameBased); // Save the prefab

        // Move everything back out of the prefab to continue editing
        foreach (GameObject g in world)
        {
            g.transform.parent = null;
        }
        saveHelper.SetActive(false);
    }

    // Outside of PLAY Mode, finalize the save
    void ApplySave()
    {
		GameObject tempGO = Instantiate(saveHelperPrefab);
		if (saveHelper != null) {
			tempGO.transform.position = saveHelper.transform.position;
			tempGO.transform.rotation = saveHelper.transform.rotation;
		} else {
			tempGO.transform.position = transform.position;
			tempGO.transform.rotation = transform.rotation;
		}
		tempGO.transform.parent = transform;
		tempGO.name = "SaveHelper";
		DestroyImmediate(saveHelper);
		saveHelper = tempGO;
        world = SceneManager.GetActiveScene().GetRootGameObjects(); // Get all root objects then kill them as long as they aren't the SaveHelper or GameMakerVR prefabs
        for (int i = world.Length - 1; i >= 0; i--)
        {
            if (world[i].tag == "SaveHelper" || world[i].tag == "GameMakerVR")
                continue;
            DestroyImmediate(world[i]);
        }
        // Bring everything out of the SaveHelper prefab
        for (int i = saveHelper.transform.childCount - 1; i >= 0; i--)
        {
            saveHelper.transform.GetChild(i).parent = null;
        }
        // Save the now empty SaveHelper Prefab
        PrefabUtility.ReplacePrefab(saveHelper, saveHelperPrefab, ReplacePrefabOptions.ConnectToPrefab | ReplacePrefabOptions.ReplaceNameBased);
        saveHelper.SetActive(true);
    }

    // Generic method to get an array of ANY object type at a specified path
    public T[] GetAtPath<T>(string path)
    {
        ArrayList al = new ArrayList(); // Holder for all of the objects to return
        string[] fileEntries = Directory.GetFiles(path); // list of all files in the folder
        foreach (string fileName in fileEntries)
        {
            int assetPathIndex = fileName.IndexOf("Assets");
            string localPath = fileName.Substring(assetPathIndex); // Localize the path

            UnityEngine.Object t = AssetDatabase.LoadAssetAtPath(localPath, typeof(T)); // Attempt to get the file (Assumes it matches the object type, else it'll still be null)

            if (t != null) // If the object matches the object type we're looking for, it won't be null
                al.Add(t); // And if that's the case, add it to the arraylist
        }
        T[] result = new T[al.Count]; // Make a more typical array of the type of object we're looking for
        for (int i = 0; i < al.Count; i++)
            result[i] = (T)al[i]; // Push all found objects of the correct type to that new array

        return result; // Return the array of correct objects found
    }
	
	// Adds a layer to the layers list and sets up the layers that said new layer should ignore.
	bool AddLayer (string layerName, int[] ignoreLayers = null, bool ignoreSelf = false) {
		// Open tag manager
		SerializedObject tagManager = new SerializedObject (AssetDatabase.LoadAllAssetsAtPath ("ProjectSettings/TagManager.asset") [0]);
		// Layers Property
		SerializedProperty layersProp = tagManager.FindProperty ("layers");
		if (!PropertyExists (layersProp, 0, maxLayers, layerName)) {
			SerializedProperty sp;
			// Start at layer 9th index -> 8 (zero based) => first 8 reserved for unity / greyed out
			for (int i = 8, j = maxLayers; i < j; i++) {
				sp = layersProp.GetArrayElementAtIndex (i);
				if (sp.stringValue == "") {
					// Assign string value to layer
					sp.stringValue = layerName;
					Debug.Log ("Layer: " + layerName + " has been added");
					
					// Add ignore layers
					if (ignoreLayers != null) {
						foreach (int ignoreIndex in ignoreLayers) {
							Physics.IgnoreLayerCollision(i, ignoreIndex, true);
						}
					}
					if (ignoreSelf) {
						Physics.IgnoreLayerCollision(i, i, true);
					}
					
					// Save settings
					tagManager.ApplyModifiedProperties ();
					return true;
				}
				if (i == j)
				Debug.Log ("All allowed layers have been filled");
			}
		} else {
		//Debug.Log ("Layer: " + layerName + " already exists");
		}
		return false;
	}
	
	private static bool PropertyExists(SerializedProperty property, int start, int end, string value) {
		for (int i = start; i < end; i++) {
			SerializedProperty t = property.GetArrayElementAtIndex (i);
			if (t.stringValue.Equals (value)) {
				return true;
			}
		}
		return false;
	}
	
	public List<GameObjectHolder> GetGameObjectHolders() {
		return goHolder;
	}
	
	public List<MaterialHolder> GetMaterialHolders() {
		return matHolder;
	}
}