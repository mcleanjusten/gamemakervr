﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System;
using UnityEngine;
using Valve.VR;


public class VREditor_Controller_UI : MonoBehaviour {
    public static VREditor_Controller_UI leftControllerUI;
    public static VREditor_Controller_UI rightControllerUI;


    private SteamVR_TrackedController thisController = null;           // The current controller
    private SteamVR_TrackedObject thisTrackedObject = null;            // The current tracked object

    //[SerializeField] private GameObject trackedDevices = null;                        // The tracked devices gameobject from SteamVR
    private GameObject model = null;
    [SerializeField] private GameObject thisTrackpad = null;                            // The tracked object linked to the controller
    [SerializeField] private SteamVR_Controller.Device thisDevice = null;               // The controller device 
    [SerializeField] private bool isRight;
    [SerializeField] private VREditor_Tools editorTools;
    
    // Right controller graphics
    [SerializeField] private GameObject grabGraphics = null;                                
    [SerializeField] private GameObject selectGraphics = null;                         
    [SerializeField] private GameObject teleportGraphics = null;                               
    [SerializeField] private GameObject redoGraphics = null;                              
    [SerializeField] private GameObject undoGraphics = null;                                 

    // Left controller graphics, config 1
    [SerializeField] private GameObject scaleGraphics = null;                               
    [SerializeField] private GameObject moveGraphics = null;                            
    [SerializeField] private GameObject dupeGraphics = null;                                
    [SerializeField] private GameObject rotateGraphics = null;                                

    // Left controller graphics, config 2
    [SerializeField] private GameObject shiftGraphics = null;                                
    [SerializeField] private GameObject prefabGraphics = null;                                
    [SerializeField] private GameObject circleSelectGraphics = null;                              
    [SerializeField] private GameObject boxSelectGraphics = null;                                 
    [SerializeField] private GameObject groupGraphics = null;                                 

    [SerializeField] private int controllerIndex = 0;                                   // The controller device index
    [SerializeField] private Vector3 scaleAmount = new Vector3(1.5f, 1.5f, 1.5f);       

    private float trackpadWidth = 0;                                                          // The width of the trackpad
    private float offsetFromCenter = 0.0124f;                                                 // The distance to offset from the x,y center of the trackpad
    private float offsetY = 0.00568f;                                                         // The distance to offset from the z center of the trackpad

    private int modelChildCount = 0;
    private bool doneRendering = false;
    private int buttonConfig = 0;                                                       // The configuration of buttons on the controller, 0 for right, 1 or 2 for left
    private int hoverSection = -1;                                                      // The section that needs to have it's size reset after being hoveredOver
    private int lastSection = 0;
    public bool trackpadFound = false;
    bool printBounds = true;
    bool trackpadClicked = false;
   

	// Use this for initialization
	void Start () {
        // Set static link to this controller
        if (isRight)
        {
            if (VREditor_Controller_UI.rightControllerUI == null)
            {
                VREditor_Controller_UI.rightControllerUI = this;
                buttonConfig = 0;
            }
            else
                Destroy(this);
        }
        else
        {
            if (VREditor_Controller_UI.leftControllerUI == null)
            {
                VREditor_Controller_UI.leftControllerUI = this;
                buttonConfig = 1;
            }
            else
                Destroy(this);
        }


        thisController = gameObject.GetComponent<SteamVR_TrackedController>();
        thisTrackedObject = gameObject.GetComponent<SteamVR_TrackedObject>();
        if(thisController == null)
        {
            print("controller not found");
        }
        if(thisTrackedObject == null)
        {
            print("tracked object not found");
        }

        model = gameObject.transform.GetChild(0).gameObject;                       // Get the model 

        print("model child count: " + model.transform.childCount);

        print(thisController.name);
        print(thisTrackedObject.name);
        controllerIndex = (int)thisTrackedObject.index;
        // print(controllerIndex);
        thisDevice = SteamVR_Controller.Input(controllerIndex);
        // print(gameObject.transform.GetChild(controllerIndex));

        

	}
	
	// Update is called once per frame
	void Update () {

        // Checks if the controllers have been rendered
        if (!doneRendering)
        {
            print("model child count: " + model.transform.childCount);
            // Check if the controller model is rendered yet
            if (model.transform.childCount > 0)
            {
                if (GetTrackPad())
                {
                    doneRendering = true;
                }
            }
                
        }
        else
        {
            // Touch detected
            if (!thisDevice.GetAxis().Equals(Vector2.zero))
            {
                int section = GetSection(thisDevice.GetAxis());
                HoverGraphic(lastSection, false);
                HoverGraphic(section, true);
                lastSection = section;
                print("pad touched");

                //Check for click, only trigger once, until pad is released
                if (thisController.padPressed && !trackpadClicked)
                {
                    ActivateTool(section);
                    trackpadClicked = true;
                }
                else if(trackpadClicked && !thisController.padPressed)
                {
                    trackpadClicked = false;
                }

            }
            else if(hoverSection != -1)
            {
                HoverGraphic(hoverSection, false);
            }
        }
        
	}


    // Get the trackpad gameobject 
    public bool GetTrackPad()
    {
        Transform tPad = model.transform.Find("trackpad");

        if (tPad != null)
        {
            print("Trackpad was found");
            thisTrackpad = tPad.gameObject;
            trackpadFound = true;
            SetTrackPadButtons();
            return true;
        }
        else
        {
            print("Trackpad was not found");
            return false;
        }
    }

    // Set the trackpad button graphics
    public void SetTrackPadButtons()
    {
        if(buttonConfig == 0)
        {
            // Grab 
            grabGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            grabGraphics.transform.localPosition = new Vector3(offsetFromCenter, 0, offsetY);
            grabGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Select
            selectGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            selectGraphics.transform.localPosition = new Vector3(0, offsetFromCenter, offsetY);
            selectGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Teleport
            teleportGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            teleportGraphics.transform.localPosition = new Vector3(-offsetFromCenter, 0, offsetY);
            teleportGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Redo
            redoGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            redoGraphics.transform.localPosition = new Vector3(0, -offsetFromCenter, offsetY);
            redoGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);
        }
        else if(buttonConfig == 1)
        {
            //Hide other potential graphics
            shiftGraphics.SetActive(false);
            prefabGraphics.SetActive(false);
            circleSelectGraphics.SetActive(false);
            groupGraphics.SetActive(false);

            // Set all current graphics to active
            scaleGraphics.SetActive(true);
            moveGraphics.SetActive(true);
            dupeGraphics.SetActive(true);
            rotateGraphics.SetActive(true);
            
            // Scale 
            scaleGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            scaleGraphics.transform.localPosition = new Vector3(offsetFromCenter, 0, offsetY);
            scaleGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Move
            moveGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            moveGraphics.transform.localPosition = new Vector3(0, offsetFromCenter, offsetY);
            moveGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Duplicate
            dupeGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            dupeGraphics.transform.localPosition = new Vector3(-offsetFromCenter, 0, offsetY);
            dupeGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Rotate
            rotateGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            rotateGraphics.transform.localPosition = new Vector3(0, -offsetFromCenter, offsetY);
            rotateGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);


            // shift 
            shiftGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            shiftGraphics.transform.localPosition = new Vector3(offsetFromCenter, 0, offsetY);
            shiftGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // prefab
            prefabGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            prefabGraphics.transform.localPosition = new Vector3(0, offsetFromCenter, offsetY);
            prefabGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Circle Select
            circleSelectGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            circleSelectGraphics.transform.localPosition = new Vector3(-offsetFromCenter, 0, offsetY);
            circleSelectGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // group
            groupGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            groupGraphics.transform.localPosition = new Vector3(0, -offsetFromCenter, offsetY);
            groupGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);
        }
        else
        {
            // Set all current graphics to active
            scaleGraphics.SetActive(false);
            moveGraphics.SetActive(false);
            dupeGraphics.SetActive(false);
            rotateGraphics.SetActive(false);

            //Hide other potential graphics
            shiftGraphics.SetActive(true);
            prefabGraphics.SetActive(true);
            circleSelectGraphics.SetActive(true);
            groupGraphics.SetActive(true);

            // shift 
            shiftGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            shiftGraphics.transform.localPosition = new Vector3(offsetFromCenter, 0, offsetY);
            shiftGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // prefab
            prefabGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            prefabGraphics.transform.localPosition = new Vector3(0, offsetFromCenter, offsetY);
            prefabGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Circle Select
            circleSelectGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            circleSelectGraphics.transform.localPosition = new Vector3(-offsetFromCenter, 0, offsetY);
            circleSelectGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // group
            groupGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            groupGraphics.transform.localPosition = new Vector3(0, -offsetFromCenter, offsetY);
            groupGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Scale 
            scaleGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            scaleGraphics.transform.localPosition = new Vector3(offsetFromCenter, 0, offsetY);
            scaleGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Move
            moveGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            moveGraphics.transform.localPosition = new Vector3(0, offsetFromCenter, offsetY);
            moveGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Duplicate
            dupeGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            dupeGraphics.transform.localPosition = new Vector3(-offsetFromCenter, 0, offsetY);
            dupeGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);

            // Rotate
            rotateGraphics.transform.parent = thisTrackpad.transform.GetChild(0);
            rotateGraphics.transform.localPosition = new Vector3(0, -offsetFromCenter, offsetY);
            rotateGraphics.transform.localRotation = new Quaternion(0, 0, 0, 0);
        }
     
    }

    // Gets the coordinate section the touch takes place in 
    public int GetSection(Vector2 touch)
    {
        // Check if the touchpad is being touched is over y = x
        if (touch.y >= touch.x)
        {
            if (touch.y >= -touch.x)
            {
                print("The point is in section 0");
                return 0;
            }
            else
            {
                if(buttonConfig == 0 && touch.y >= 0)
                {
                    print("The point is in section 4");
                    return 4;
                }
                else if(buttonConfig == 0 && touch.y < 0 )
                {
                    print("The point is in section 3");
                    return 3;
                }
                else if(buttonConfig != 0)
                {
                    print("The point is in section 3");
                    return 3;
                }
            }

        }
        else
        {
            if(touch.y >= -touch.x)
            {
                print("The point is section 1");
                return 1;
            }
            else
            {
                if(buttonConfig == 2 && touch.x >= 0)
                {
                    print("The point is in section 2");
                    return 2;
                }
                else if (buttonConfig == 2 && touch.x < 0)
                {
                    print("The point is in section 3");
                    return 3;
                }
                else if (buttonConfig != 2)
                {
                    print("The point is in section 2");
                    return 2;
                }
            }
        }
        return 0;
    }


    public bool GrabMode()
    {
        if (leftControllerUI!= null && leftControllerUI.buttonConfig == 1) {
            return true;
		} else {
            return false;
		}
    }


    // Make the graphic inrease insize while hovered over 
    public void HoverGraphic(int position, bool grow)
    {

        Vector3 newScaleAmount = scaleAmount;
        if (!grow)
        {
            newScaleAmount = new Vector3(1, 1, 1);
        }

        if(position == 0)
        {
            if(buttonConfig == 0)
            {
                grabGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
            else if(buttonConfig == 1)
            {
                scaleGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
            else
            {
                scaleGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
        }
        if (position == 1)
        {
            if (buttonConfig == 0)
            {
                selectGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
            else if (buttonConfig == 1)
            {
                moveGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
            else
            {
                prefabGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
        }
        if (position == 2)
        {
            if (buttonConfig == 0)
            {
                teleportGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
            else if (buttonConfig == 1)
            {
                dupeGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
            else
            {
                circleSelectGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
        }
        if (position == 3)
        {
            if (buttonConfig == 0)
            {
                redoGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
            else if (buttonConfig == 1)
            {
                rotateGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
            else
            {
                boxSelectGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
        }
        if (position == 4)
        {
            if (buttonConfig == 0)
            {
                undoGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
            else
            {
                groupGraphics.transform.GetChild(0).localScale = newScaleAmount;
            }
        }
        hoverSection = position;
    }

    // Set the button config for a controller
    public void SetButtonConfig(int layout)
    {
        buttonConfig = layout;
        Debug.Log("Changing Config");
        SetTrackPadButtons();
    }

    //Perform the buttonClick action
    public void ActivateTool(int section)
    {
        if (isRight)
        {
            // Check that the VREditor_Tools is valid
            if (editorTools != null)
            {
                switch (section)
                {
                    case 0:     // Grab button
                        editorTools.ChangeTool(0);         // Change the tool on the right hand
                        leftControllerUI.SetButtonConfig(1);
                        break;
                    case 1:     // Select button
                        // Do something
                        Debug.Log("Activate select");
                        leftControllerUI.SetButtonConfig(2);
                        break;
                    case 2:     // Teleport button
                        // Do something 
                        break;
                    case 3:     // Redo button
                        editorTools.PerformRedo();
                        break;
                    case 4:     // Undo button
                        editorTools.PerformUndo();
                        break;
                }
            }
        }
        else
        {
            // Check editor tools is valid
            if (editorTools != null)
            {
                if (buttonConfig == 1)
                {
                    switch (section)
                    {
                        case 0:     // Scale button
                            editorTools.ChangeTool(3);
                            break;
                        case 1:     // Move button
                            editorTools.ChangeTool(1);
                            break;
                        case 2:     // Duplicate Button
                                    // Do something 
                            break;
                        case 3:     // Rotate Button
                            editorTools.ChangeTool(2);
                            break;
                    }
                }
                else
                {
                    switch (section)
                    {
                        case 0:     // Shift button
  
                            break;
                        case 1:     // Make Prefab button

                            break;
                        case 2:     // Circle grouping button
                                    // Do something 
                            break;
                        case 3:     // Box grouping button

                            break;
                        case 4:     // Group/Ungroup button

                            break;
                    }
                }
            }
        }
    }
}
