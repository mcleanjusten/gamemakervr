﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class ObjectMenuController : MonoBehaviour {
    public static ObjectMenuController objectMenuController = null;
    private GameObject selectedGameObject = null;               // Game object to modify
	public VREditor_Tools tools;								// Reference to VREditor_Tools to keep track of currentTool
	public Transform handles;									// Transform for the handles to be used by VREditor_Tools.cs
	public bool useLocalSpace = false;							// Whether or not we're in local space or global space
    public bool isEnabled = false;                             // Is the menu currently enabled
    [SerializeField] private GameObject menuCanvas = null;      // The canvas of the object menu
    [SerializeField] private GameObject worldMenuCanvas = null;      // The canvas of the world object menu
    [SerializeField] private GameObject controllerMenuCanvas = null;    // The canvas of the controller object menu
    [SerializeField] private GameObject controllerMenuHolder = null;    // Game object that holds the controller menu
    [SerializeField] private Camera hmdCamera = null;           // The main camera ( Head (eyes) )

    private Collider selectedObjectCollider = null;             // The collider of the selected object
    public SteamVR_TrackedController current_controller;       // The current controller that the menu is stuck too, by default the left controller

    // These value should be changed to radio buttons in the editor gui, only one should be true at any time
    // Decides the direction the menu should be displayed next to a selected gameobject
    [Header("World Menu Placement")]
    [SerializeField] private bool displayLeft = true;
    [SerializeField] private bool displayRight = false;
    [SerializeField] private bool displayTop = false;
    [SerializeField] private bool displayBottom = false;

    [Header("World Menu Distance")]
    [SerializeField] private float distFromObject = 1f;     // The distance from the selected object the menu should be
    [SerializeField] private float menuHeight = 1f;         // How high the menu should be off the ground if on the side of an object

    [Header("Controller Menu Options")]
    [SerializeField] private bool controllerMenuMode = false;   // Should the menu spawn above the controller instead of next to the game object
    [SerializeField] private GameObject controllerToStickToo;   // A reference to the controller the controller menu needs to stick too 
    [SerializeField] private float heightAboveController = 0.25f;   // The height the menu canvas is above the controller
    [SerializeField] private float distanceBehindController = 0.07f;    // The distance the menu should be behind the controller

    [Header("Canvas Scale")]
    [SerializeField] private Vector3 objectScale = new Vector3(.003f, .003f, 1);         // The scale of the canvas if next to an object
    // [SerializeField] private Vector3 controllerScale = new Vector3(.001f, .001f, 1);     // The scale of the canvas if attatched to the controller

    // Text fields for the different controller canvas items, needs to assigned from the unity editor
    [Header("Controller Menu Info Fields")]
    [SerializeField] private GameObject basicPanel = null;      // The panel for the basic info
    [SerializeField] private GameObject visualPanel = null;     // The panel for the visual info
    [SerializeField] private GameObject componentPanel = null;  // The panel for the component info
    [SerializeField] private Text[] controllerNameField ;
    [SerializeField] private Text controllerRotationField;
    [SerializeField] private Text controllerScaleField;
    [SerializeField] private Text controllerPositionField ;
    [SerializeField] private Text controllerLayerField ;
    [SerializeField] private Text controllerMaterialField ;
    [SerializeField] private GameObject controllerColorSphere;
    [SerializeField] private GameObject materialListTemplate ;
    [SerializeField] private GameObject materialListGrid ;
    [SerializeField] private GameObject componentsCollider;
    [SerializeField] private GameObject componentsRigidBody;
    //[SerializeField] private string materialsPath ;
    [SerializeField] private GameObject VREditorMenu ;
    [SerializeField] private GameObject scriptsListTemplate;
    [SerializeField] private GameObject scriptsListGrid;
    [SerializeField] private GameObject addColliderButton;
    [SerializeField] private GameObject removeColliderButton;
    [SerializeField] private GameObject addRigidBodyButton;
    [SerializeField] private GameObject removeRigidbodyButton;
    [SerializeField] private GameObject addScriptsGrid;



    // Text fields for the different world canvas items, needs to assigned from the unity editor
    [Header("World Menu Info Fields")]
    [SerializeField] private Text[] worldNameField;
    [SerializeField] private Text worldPositionField;
    [SerializeField] private Text worldLayerField;
    [SerializeField] private Text worldMaterialField;


    // The actual fields to use
    private Text[] objectNameField;
    private Text objectPositionField;
    private Text objectLayerField;
    private Text objectMaterialField;


    // Use this for initialization
    void Start() {
        // Make a static reference in this class to itself so it can be easily called from any other class
        if( objectMenuController == null)
        {
            objectMenuController = this;
        }
        else
        {
            Destroy(this);

        }


        //Set the controller canvas variables
        if (controllerMenuMode)
        {
            menuCanvas = controllerMenuCanvas;      // Switch the type canvas used 
            objectNameField = controllerNameField;
            objectPositionField = controllerPositionField;
            objectLayerField = controllerLayerField;
            objectMaterialField = controllerMaterialField;
        }
        else
        {
            //Set the world canvas variables
            menuCanvas = worldMenuCanvas;
            objectNameField = worldNameField;
            objectPositionField = worldPositionField;
            objectLayerField = worldLayerField;
            objectMaterialField = worldMaterialField;
        }   

        // Display an error if the object menu gameObject is not found at start up
        if (menuCanvas == null)
        {
            Debug.LogError(" Object menu gameobject not seleced");
        }

        

        // Get the Hmd Camera 
        if (hmdCamera == null)
        {
            hmdCamera = GameObject.Find("Main Camera (eye)").GetComponent<Camera>();
            if(hmdCamera == null)
            {
                Debug.Log("Hmd Camera Not Found");
                hmdCamera = Camera.main;
            }
        }

        if( objectNameField == null || objectPositionField == null || objectLayerField == null || objectMaterialField == null)
        {
            Debug.Log("Canvas Info Fields are missing");
        }
    }
	
	// LateUpdate so all calculations are done when this happens (Last thing before the Draw Calls are initiated)
	void LateUpdate () {
		if (selectedGameObject != null) { // If we have a selected object, show the handles
			if (handles.GetComponent<VREditor_HandleController>().selectedObject != selectedGameObject.transform.parent) { // Set the selectedObject of the handle controller
				handles.GetComponent<VREditor_HandleController>().selectedObject = selectedGameObject.transform.parent;
			}
			if (!handles.gameObject.activeInHierarchy)
				handles.gameObject.SetActive(true);
			handles.position = selectedGameObject.transform.parent.position; // Set the handles position to the object
			
			if (useLocalSpace || tools.CurrentTool() == 3) // Use local rotation if we're in localSpace OR using the Scale tool (Scale should ALWAYS be done in local coordinates)
				handles.rotation = selectedGameObject.transform.parent.rotation;
			else
				handles.rotation = Quaternion.identity;
			
			SetMenuPositionText(); // Update the menu position text
		} else if (handles.gameObject.activeInHierarchy) { // If no object is selected but the handles are still visible, hide the handles and don't update further
			handles.gameObject.SetActive(false);
			handles.GetComponent<VREditor_HandleController>().selectedObject = null; // null out the selectedObject of the handle controller
		}
	}

    // Disables the object menu if it is already enabled or enables it if it is not enabled
    public void InvokeObjectMenu(GameObject selectedObject)
    {
        current_controller = controllerToStickToo.GetComponent<SteamVR_TrackedController>();        // Get the current steamVR tracked controller

        // If the menu is already up on an object and it is clicked again hide the menu
        if (isEnabled && selectedObject == selectedGameObject)
        {
			selectedGameObject = null;
            DisableObjectMenu();
        }
        else
        {
            // Show the menu for the selected object
            EnableObjectMenu(selectedObject);
        }
    }

    // Disables the object menu
    public void DisableObjectMenu()
    {
        isEnabled = false;
        menuCanvas.SetActive(false);        //Disable the menu gameObject
    }


    // Enables the object menu for specified gameObject, selectedObject should be the GameMakerVR_Interactable Child
    public void EnableObjectMenu(GameObject selectedObject)
    {
        isEnabled = true;
        selectedGameObject = selectedObject;
        selectedObjectCollider = selectedObject.GetComponent<Collider>();
        SetMenuText();                                                      // Set the menu text
        menuCanvas.SetActive(true);                                         // Activate the menu
        SetComponentButtons();
        // Set the canvas position
        if (!controllerMenuMode)
        {
            SetWorldMenuPosition();
        }
        else
        {
            SetControllerMenuPosition();
        }
    }


    // Sets the menus position to the selected direction of the center of the selected game object
    private void SetWorldMenuPosition()
    {
        // Set the menu scale
        menuCanvas.transform.localScale = objectScale;

        // Move the menu canvas to the center of the gameObject
        menuCanvas.transform.position = selectedGameObject.GetComponent<Collider>().bounds.center;

        // Set the menu to face the camera
        menuCanvas.transform.rotation = Quaternion.LookRotation(menuCanvas.transform.position - hmdCamera.transform.position);

        Debug.Log("Selected Object bounds are : " + selectedObjectCollider.bounds.ToString());

        // Check if the menu should be put to the left, right , top, or bottom of the selected game object
        if (displayLeft)
        {
            menuCanvas.transform.localPosition += -menuCanvas.transform.right * ( distFromObject + System.Math.Max(selectedObjectCollider.bounds.extents.x, selectedObjectCollider.bounds.extents.z) / 2 + ( menuCanvas.GetComponent<RectTransform>().rect.width * menuCanvas.GetComponent<RectTransform>().lossyScale.x) / 2);
            
            // Set the menu height so it is not in the ground
            Vector3 currentPos = menuCanvas.transform.position;
            currentPos.y = (menuCanvas.GetComponent<RectTransform>().rect.height * menuCanvas.GetComponent<RectTransform>().lossyScale.y) / 2 + menuHeight;
            menuCanvas.transform.position = currentPos;
        }
        else if(displayRight)
        {
            menuCanvas.transform.localPosition += menuCanvas.transform.right * (distFromObject + System.Math.Max(selectedObjectCollider.bounds.extents.x, selectedObjectCollider.bounds.extents.z) / 2 + (menuCanvas.GetComponent<RectTransform>().rect.width * menuCanvas.GetComponent<RectTransform>().lossyScale.x) / 2);

            // Set the menu height so it is not in the ground
            Vector3 currentPos = menuCanvas.transform.position;
            currentPos.y = (menuCanvas.GetComponent<RectTransform>().rect.height * menuCanvas.GetComponent<RectTransform>().lossyScale.y) / 2 + menuHeight;
            menuCanvas.transform.position = currentPos;
        }
        else if (displayTop)
        {
            menuCanvas.transform.localPosition += menuCanvas.transform.up * (distFromObject + selectedObjectCollider.bounds.extents.y / 2 + (menuCanvas.GetComponent<RectTransform>().rect.height * menuCanvas.GetComponent<RectTransform>().lossyScale.y) / 2);
            //menuCanvas.transform.localPosition += menuCanvas.transform.up * (selectedObjectCollider.bounds.max.z + distFromObject + canvasBoxCollider.bounds.max.y);
        }
        else if (displayBottom)
        {
            menuCanvas.transform.localPosition += -menuCanvas.transform.up * (distFromObject + selectedObjectCollider.bounds.extents.y / 2 + (menuCanvas.GetComponent<RectTransform>().rect.height * menuCanvas.GetComponent<RectTransform>().lossyScale.y) / 2);
        }

        // Set the menu to face the camera after it has been moved next to the selecte game object
        // menuCanvas.transform.rotation = Quaternion.Euler(hmdCamera.transform.rotation.eulerAngles.x, hmdCamera.transform.rotation.eulerAngles.y, 0);
        menuCanvas.transform.rotation = Quaternion.LookRotation(menuCanvas.transform.position - hmdCamera.transform.position);
    }


    // Not yet implemented
    public void SetControllerMenuPosition()
    {
        // Temporarily have it just set the menu next to the object
        // SetWorldMenuPosition();

        // Parent the menu to the controller
        controllerMenuHolder.transform.parent = controllerToStickToo.transform;

        // Center the canvas
        controllerMenuHolder.transform.localPosition = Vector3.zero;

        // Set the menu to face the camera
        //menuCanvas.transform.rotation = Quaternion.LookRotation(menuCanvas.transform.position - hmdCamera.transform.position);

        controllerMenuHolder.transform.rotation = controllerToStickToo.transform.rotation;

        // Rotate on the x-axis by 60 degrees
        controllerMenuHolder.transform.localRotation = Quaternion.Euler(60, 0, 0);

        // Move the canvas to the correct position
        menuCanvas.transform.localPosition = new Vector3(0, heightAboveController, distanceBehindController);

    }
	
	// Used for updating Transform information in real time
	void SetMenuPositionText () {
		GameObject parentObject = selectedGameObject.transform.parent.gameObject;
        objectPositionField.text = parentObject.transform.position.ToString();
        controllerScaleField.text = parentObject.transform.localScale.ToString();
        controllerRotationField.text = parentObject.transform.rotation.eulerAngles.ToString();
	}
	
    // Fills out menu with object information
    private void SetMenuText()
    {
        GameObject parentObject = selectedGameObject.transform.parent.gameObject;
		foreach (Text t in objectNameField) {
			t.text = parentObject.name;
		}
        objectPositionField.text = parentObject.transform.position.ToString();
        objectLayerField.text = LayerMask.LayerToName(parentObject.layer).ToString();
        

        // Controller text fields
        controllerScaleField.text = parentObject.transform.localScale.ToString();
        controllerRotationField.text = parentObject.transform.rotation.eulerAngles.ToString();
        if(parentObject.GetComponent<Renderer>() != null)
        {
            if(parentObject.GetComponent<Renderer>().materials.Length > 0)
            {
                objectMaterialField.text = parentObject.GetComponent<Renderer>().sharedMaterial.name;
                controllerColorSphere.GetComponent<Renderer>().sharedMaterial = parentObject.GetComponent<Renderer>().sharedMaterial;
            }
            
        }
        
        GetMaterials();		// Set the material icons
    }


    // Gets the different materials available for the object
    public void GetMaterials()
    {
        // Object[] materials = Resources.FindObjectsOfTypeAll(typeof(Material));           // Get all the material objects
        //Material[] materials = gameObject.GetComponent<VREditorController>().GetAtPath<Material>(Application.dataPath + materialsPath);
		VREditorController.MaterialHolder[] materials = gameObject.GetComponent<VREditorController>().GetMaterialHolders().ToArray();
        
        // Add an icon and name for each material to the materials panel
        for (int i = 0; i < materials.Length; ++i)
        {
            // Make a new instance of the material template
            GameObject matIcon = Instantiate(materialListTemplate);
            matIcon.transform.GetChild(0).GetComponent<Text>().text = materials[i].material.name;                     // Set the name for the icon
            matIcon.transform.GetChild(1).GetComponent<Renderer>().sharedMaterial = materials[i].material;            // Set the name for the icon

            // Set up the button for each color
            Button button = matIcon.GetComponent<Button>();
            button.onClick.AddListener( delegate () { this.SetSelectedObjectMaterial(matIcon.transform.GetChild(1).GetComponent<Renderer>().sharedMaterial); } ) ;

            matIcon.SetActive(true);                                                                                  // Activate the icon
            matIcon.transform.SetParent(materialListTemplate.transform.parent);                                       // Set the icon parent
            matIcon.transform.localScale = materialListTemplate.transform.localScale;

        }


    }

    // Gets the selected objects material
    public Material GetSelectedObjectMaterial()
    {
        return selectedGameObject.transform.parent.GetComponent<Material>();
    }

    // Sets the selected objects material
    public void SetSelectedObjectMaterial(Material newMat)
    {
		if (selectedGameObject.transform.parent.GetComponent<Renderer>() == null)
			return;
        selectedGameObject.transform.parent.GetComponent<Renderer>().sharedMaterial = newMat;
		controllerColorSphere.GetComponent<Renderer>().sharedMaterial = selectedGameObject.transform.parent.GetComponent<Renderer>().sharedMaterial;        // Updates the color sphere after changing the object color
        objectMaterialField.text = selectedGameObject.transform.parent.GetComponent<Renderer>().sharedMaterial.name;                                        // Updates the color name



    }
	
    // Set the controller for the menu to stick too
    public void SetControllerToFollow(GameObject controller)
    {
        controllerToStickToo = controller;
    }


    // Delete the selected gameObject
    public void DeleteSelectedObject()
    {
        Destroy(selectedGameObject.transform.root.gameObject);        // Delete the gameObject
    }

    // Set Which buttons to show for the component menu
    public void SetComponentButtons()
    {
        // Set add or remove Collider button
        if(selectedGameObject.transform.parent.GetComponent<Collider>() != null)
        {
            addColliderButton.SetActive(false);
            removeColliderButton.SetActive(true);
        }
        else
        {
            addColliderButton.SetActive(true);
            removeColliderButton.SetActive(false);
        }

        // Set add or remove collider button
        if(selectedGameObject.transform.parent.GetComponent<Rigidbody>() != null)
        {
            addRigidBodyButton.SetActive(false);
            removeRigidbodyButton.SetActive(true);
        }
        else
        {
            addRigidBodyButton.SetActive(true);
            removeRigidbodyButton.SetActive(false);
        }


    }

    // Remove a Collider from the selected object
    public void RemoveCollider()
    {
        
    }
}
